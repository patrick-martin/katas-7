//array and callback function to unit test newForEach function
// let array1 = [1, 2, 3, 4]
// let callback1 = function(item) {
//         console.log(item)
//     }
//Tested function and it works!
//**REMEMBER to include a return statement.  What would return statement be?

const arr1 = [1,2,3,4]
const multiplyBy2 = (element) => element * 2;
const multiplyBy2NEW = (element)=>console.log(element*2);
function newForEach(array, callback) {
    //let answer = '';
    for (let i = 0; i < array.length; i++) {
       // answer += `${callback(array[i])}`;
       callback(array[i]);
    }
   // return answer;
}
newForEach(arr1, multiplyBy2NEW) //returns correct elements in console.  How to include in return statement?  Push answers to empty string?

//exercise 2-- works.  
//.map() RETURNs a NEW ARRAY based on some condition applied to each element in the callback function.
function newMap(array, callback) {
    let newArray = [];
    for (let i = 0; i < array.length; i++) {
        let arrayItem = array[i];
        let newArrayItem = callback(arrayItem);
        newArray.push(newArrayItem);
    }
    return newArray;
}
newMap(arr1, multiplyBy2)

//exercise 3
//.some() loops through each element and returns a boolean depending on whether ONE element passes condition determined by callback function.
let arr2 = [1,2,3,4];
function checkForEvens(item){
    let answer = '';
    if (item % 2 === 0){
        answer = true;
    }
    else {
        answer = false;
    }
    return answer;
}

function newSome(array, callback) {
    for (i = 0; i < array.length; i++) {
        if (callback(array[i]) === 'true') {
            return true;
        }
        else {
            void 0;
        }
    }
    //return false;
}

//.some() loops through an array, performs callback function on each item in an array until a condition is met (as outlined in callback).  Once condition is met, callback returns 'true', which is returned 

//exercise 4
//.find returns the VALUE of an array element that meets a specific condition as determined by the callback function
function newFind(array, callback) {
    for (i = 0; i < array.length; i++) {
        if (callback(array[i]) === 'true') {
            return array[i];
        } else {
            return false;
        }
    }
}

//exercise 5
//.findIndex returns the INDEX of the first array element which meets condition of callback function

function newFindIndex(array, callback) {
    for (i = 0; i < array.length; i++) {
        if (callback(array[i]) === 'true') {
            return i;
        }
    }
}

//exercise 6
//.every() returns boolean value; loops through each element and tests whether EVERY element passes a condition.
//if an element doesn't pass a condition, the value 'false' is returned
function newEvery(array, callback) {
    for (i = 0; i < array.length; i++) {
        if (callback(array[i]) === 'false') {
            return false;
        }
    }
}

//exercise 7
//.filter() creates a NEW ARRAY for each element that passes a condition determined by callback function
function newFilter(array, callback) {
    let newArray = [];
    for (i = 0; i < array.length; i++) {
        if (callback(array[i]) === 'true') {
            newArray.push(callback(array[i]))
        }
    }
    return newArray;
}